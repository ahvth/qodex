import os
import csv
from markdown2 import markdown
from jinja2 import Environment, FileSystemLoader

# global vars
site_prefix = "http://qodex.org/"
content_prefix = "content/"
story_prefix = "stories/"
tag_prefix = "tags/"

# metadata structure
# title: display title + building block for filename
# url: external/internal permalink
# origin: nation of origin for article (headlines only)
# story: related to "ongoing" section of title page, links to ongoing "story"
# tags: assorted tags, TBD 

class headline:
    def __init__(self, title, url, story, tags):
        self.title = title
        self.url = url
        self.story = story
        self.tags = tags # list item

class post:
    def __init__(self, title, url, story, tags):
        self.title = title
        self.url = url
        self.story = story
        self.tags = tags # list item

class story:
    def __init__(self, title, url):
        self.title = title
        self.url = url

class tag:
    def __init__(self, title, url):
        self.title = title
        self.url = url

def get_headlines():
    # TODO: use only new headlines
    headlines=[]
    story_index=[]
    stories=[]
    tags=[]
    tag_index=[]
    with open('headlines/headlines.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            headlines.append(headline(row[0], row[1], row[2], row[3]))
    for h in headlines:
        for t in h.tags.split("&"):
            if t not in tag_index:
                tag_index.append(t)
                tag_url=tag_prefix+t.replace(" ", "_")+".html"
                tags.append(tag(t, tag_url))
        if h.story not in story_index:
            story_index.append(h.story)
            story_url = story_prefix+h.story.replace(" ", "_")+".html"
            stories.append(story(h.story, story_url))
    return headlines, story_index, stories, tags, tag_index

# generates new posts and related story pages
def gen_posts(template_env, story_index, stories, tags, tag_index):
    posts=[]
    for file in os.walk("posts"):
        postlist = file[2]
    # return postlist
    for p in postlist:
        metadata = {}
        with open("posts/"+p) as f:
            content = f.read()
        content = content.split("---\n")
        header = content[0]
        for line in header.split("\n"):
            if line and ": " in line:
                name, value = line.split(": ")
                metadata[name] = value
        post_title = metadata["title"]
        story_title = metadata["story"]
        taglist = metadata["tags"]
        filename = post_title.replace(" ", "_")+".html"
        story_filename = story_title.replace(" ", "_")+".html"
        url = content_prefix+filename
        story_url = story_prefix+story_filename
        body = markdown(content[1], extras=['fenced-code-blocks', 'code-friendly'])
        posts.append(post(post_title, site_prefix+url, story_title, taglist))
        for t in taglist.split("&"):
            if t not in tag_index:
                tag_index.append(t)
                tag_url=tag_prefix+t.replace(" ", "_")+".html"
                tags.append(tag(t, tag_url))
        if story_title not in story_index:
            story_index.append(story_title)
            stories.append(story(story_title, site_prefix+story_url))
        with open("content/"+filename, 'w') as output_file:
            template = template_env.get_template('post.html')
            output_file.write(template.render(body=body, title=post_title, story=story_title))
        
    return posts, stories, tags

def gen_tag_pages(template_env, tags, posts, headlines):
    for tag in tags:
        tag_filename = tag.title.replace(" ", "_")+".html"
        with open("tags/"+tag_filename, 'w') as output_file:
            template = template_env.get_template("tag.html")
            output_file.write(template.render(tag=tag, posts=posts, headlines=headlines))
def gen_story_pages(template_env, stories, posts, headlines):
    for story in stories:
        story_filename = story.title.replace(" ", "_")+".html"
        with open("stories/"+story_filename, 'w') as output_file:
            template = template_env.get_template('story.html')
            output_file.write(template.render(story=story, posts=posts, headlines=headlines))

def gen_site():
    template_env = Environment(loader=FileSystemLoader(searchpath='templates/'))
    headlines, story_index, stories, tags, tag_index = get_headlines()
    posts, stories, tags = gen_posts(template_env, story_index, stories, tags, tag_index)
    gen_story_pages(template_env, stories, posts, headlines)
    gen_tag_pages(template_env, tags, posts, headlines)
    with open('index.html', 'w') as output_file:
        template = template_env.get_template('index.html')
        output_file.write(template.render(headlines=headlines, posts=posts, stories=stories, tags=tags))

gen_site()
